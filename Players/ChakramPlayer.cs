﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;
using Terraria.ModLoader.IO;
using Terraria.GameInput;
using static Terraria.ModLoader.ModContent;
using CrossCode.Projectiles;

namespace CrossCode.Players
{
    class ChakramPlayer : ModPlayer
    {
        #region Properties
        public static ChakramPlayer ModPlayer(Player player) {
            return player.GetModPlayer<ChakramPlayer>();
        }

        // Item stats modifiers
        public float ChakramDmgAdd { get; set; }
        public float ChakramDmgMult { get; set; }
        public float ChakramKnockback { get; set; }
        public int ChakramCrit { get; set; }
        // Active properties
        public bool IsSpecialKeyPressed { get; private set; }
        public string SelectedElement { get; set; }
        public int SkillPointsCount { get; set; }
        // Passive properties
        public Dictionary<string, bool> UnlockedElements { get; set; } // key = element -> value = true for unlocked, false for locked
        public Dictionary<string, int> ArtProjectiles { get; set; } // ie for key = "SpecialNeutralMelee" -> value = projectile.id, null if locked
        public Dictionary<int, int> ForkOpposite { get; set; } // key = projectile.id -> value the other projectile.id from fork
        public Dictionary<int, bool> SpawnAtPlayer { get; set; } // key = projectile.id -> value = projectileName.SpawnAtPlayer

        #endregion

        #region Init fields and properties, save and load them to tagcompund
        
        public override void Initialize() {
            IsSpecialKeyPressed = false;
            SelectedElement = "Neutral";
            SkillPointsCount = 0;
            UnlockedElements = new Dictionary<string, bool>() {
                {"Neutral", true},
                {"Fire", false},
                {"Cold", false},
                {"Shock", false},
                {"Wave", false}
            };

            int vrp = ProjectileType<VRP>();
            int spinDance = ProjectileType<SpinDanceProjectile>();
            ArtProjectiles = new Dictionary<string, int>() {
                {"NormalNeutralMelee", 0},
                {"NormalNeutralRanged", vrp},
                {"EmpoweredNeutralRanged", vrp},
                {"SpecialNeutralMelee", spinDance}, // {"SpecialNeutralMelee", -1}
                {"SpecialNeutralRanged", -1}
            };

            /*ForkOpposite = new Dictionary<int, int>() {
                {spinDance, saw}
            };*/

            SpawnAtPlayer = new Dictionary<int, bool>() {
                {vrp, VRP.SpawnAtPlayer},
                {spinDance, SpinDanceProjectile.SpawnAtPlayer}
            };
        }

        public override TagCompound Save() {
            TagCompound tag = new TagCompound();

            tag.Add("SelectedElement", SelectedElement);

            tag.Add("SkillPointsCount", SkillPointsCount);

            // Save unlocked elements from the dict to tags
            foreach (string element in UnlockedElements.Keys.ToList()) {
                tag.Add(element, UnlockedElements[element]);
            }

            return tag;
        }

        public override void Load(TagCompound tag) {
            if (tag.ContainsKey("SelectedElement")) {
                SelectedElement = tag.GetString("SelectedElement");
            }
            
            SkillPointsCount = tag.GetInt("SkillPointsCount");

            // Load unlocked elements to the dict from tags
            foreach (string element in UnlockedElements.Keys.ToList()) {
                UnlockedElements[element] = tag.GetBool(element);
            }
            UnlockedElements["Neutral"] = true; // neutral is always unlocked

        }

        #endregion

        public override void ResetEffects() {
            ResetVariables();
        }

        public override void UpdateDead() {
            ResetVariables();
        }

        private void ResetVariables() {
            ChakramDmgAdd = 0f;
            ChakramDmgMult = 1f;
            ChakramKnockback = 0f;
            ChakramCrit = 0;
    }

        public override void ProcessTriggers(TriggersSet triggersSet) {
            // is the player holding the special action key or not
            if(CrossCode.SpecialActionHotKey.Current) {
                IsSpecialKeyPressed = true;
            } else {
                IsSpecialKeyPressed = false;
            }
        }
    }
}
