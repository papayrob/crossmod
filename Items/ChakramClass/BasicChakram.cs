﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using static Terraria.ModLoader.ModContent;

namespace CrossCode.Items.ChakramClass
{
    class BasicChakram : ChakramItem
    {
		public override void SetStaticDefaults() {
			DisplayName.SetDefault("Basic Chakram");
		}

		public override void DerivedSetDefaults() {
			item.damage = 20;
			item.crit = 20;
			item.knockBack = 2f;
			item.rare = 15;
			item.useTime = 15;
			item.useAnimation = 15;
			item.shootSpeed = 30f;

			EmpoweredTreshold = 100;
			EmpoweredShotMult = 2f;
		}
	}
}
