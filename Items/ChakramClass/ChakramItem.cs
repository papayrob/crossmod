using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using static Terraria.ModLoader.ModContent;
using Microsoft.Xna.Framework;
using CrossCode.Players;

namespace CrossCode.Items.ChakramClass
{
    // Base for chakrams
    public abstract class ChakramItem : ModItem
    {
        // Static properties
        public static int MeleeDist { get; } = 180; // distance from the player to which the chakrams will be melee
        private static Dictionary<string, bool> SpawnAtPlayerDict { get; }

        // Non-static properties
        public string Mode { get; private set; }
        private short timer;
        public int EmpoweredTreshold { private get; set; }
        public float EmpoweredShotMult { get; set; }

        #region Modifying chakram damage from vanilla
        // Called when the mod loads, so our changes are added to the game
        public static void AddHacks() {
            // Sets the weapon damage and knockback to melee or ranged so that it afflicts chakram damage
            On.Terraria.Player.GetWeaponDamage += PlayerOnGetWeaponDamage;
            On.Terraria.Player.GetWeaponKnockback += PlayerOnGetWeaponKnockback;
        }

        private static int PlayerOnGetWeaponDamage(On.Terraria.Player.orig_GetWeaponDamage orig, Player self, Item sitem) {
            bool isChakram = sitem.type == ItemType<BasicChakram>();
            float cursorDist = Vector2.Distance(Main.MouseWorld, self.position);

            // Sets the chakram to melee or ranged mode based on distance of the cursor from the player
            if (isChakram && cursorDist < MeleeDist) {
                sitem.melee = true;
            } else if (isChakram && cursorDist >= MeleeDist) {
                sitem.ranged = true;
            }

            int dmg = orig(self, sitem);
            if (isChakram) {
                sitem.melee = false;
                sitem.ranged = false;
            }
            return dmg;
        }

        private static float PlayerOnGetWeaponKnockback(On.Terraria.Player.orig_GetWeaponKnockback orig, Player self, Item sitem, float knockback) {
            bool isChakram = sitem.type == ItemType<BasicChakram>();
            float cursorDist = Vector2.Distance(Main.MouseWorld, self.position);

            // Sets the chakram to melee or ranged mode based on distance of the cursor from the player
            if (isChakram && cursorDist < MeleeDist) {
                sitem.melee = true;
            } else if (isChakram && cursorDist >= MeleeDist) {
                sitem.ranged = true;
            }

            float kb = orig(self, sitem, knockback);
            if (isChakram) {
                sitem.melee = false;
                sitem.ranged = false;
            }
            return kb;
        }
        #endregion

        #region Set common properties and values
        // Sets defaults that are common for all chakrams
        public sealed override void SetDefaults() {
            EmpoweredTreshold = 100;
            EmpoweredShotMult = 1.5f;
            // Derived classes set defaults
            DerivedSetDefaults();
            // Common defaults
            // set damage types to false to indicate it's not a vanilla class
            item.melee = false;
            item.ranged = false;
            item.magic = false;
            item.thrown = false;
            item.summon = false;
            // set item properties
            item.width = 40;
            item.height = 40;
            item.autoReuse = true;
            item.useStyle = 1;
            item.useTurn = true;
            item.noMelee = true;
            item.noUseGraphic = false;
            // initialize class fields
            timer = 0;
        }

        // Set defaults for derived classes
        public virtual void DerivedSetDefaults() {}

        // Sets chakram stats and projectile based on current mode, element and special key press
        public override sealed void ModifyWeaponDamage(Player player, ref float add, ref float mult, ref float flat) {
            ChakramPlayer modPlayer = ChakramPlayer.ModPlayer(player);
            bool empowered = player.HasBuff(BuffType<Buffs.EmpoweredShotBuff>()) && string.Equals(Mode, "Ranged");

            // Shoot the right projectile
            string art = (modPlayer.IsSpecialKeyPressed ? "Special" : (empowered ? "Empowered" : "Normal") ) + modPlayer.SelectedElement + (Mode ?? "Melee");
            int artProjectile = modPlayer.ArtProjectiles[art];

            if (artProjectile != -1) {
                item.shoot = artProjectile;
            } else {
                art = "Normal" + modPlayer.SelectedElement + Mode;
                item.shoot = modPlayer.ArtProjectiles[art];
            }

            // Modifies base damage
            add += modPlayer.ChakramDmgAdd;
            mult *= modPlayer.ChakramDmgMult;

            // Derived classes modify damage
            DerivedModifyWeaponDamage(player, ref add, ref mult, ref flat);
        }

        // Modify damage for derived classes
        public virtual void DerivedModifyWeaponDamage(Player player, ref float add, ref float mult, ref float flat) {}

        public override sealed void GetWeaponKnockback(Player player, ref float knockback) {
            // Adds knockback bonuses
            knockback += ChakramPlayer.ModPlayer(player).ChakramKnockback;
        }

        public override sealed void GetWeaponCrit(Player player, ref int crit) {
            // Adds crit bonuses
            crit += ChakramPlayer.ModPlayer(player).ChakramCrit;
        }
        #endregion

        public override void UpdateInventory(Player player) {
            // Removes empowered buff when the player is not holding a chakram
            Item heldItem = player.HeldItem;
            int empoweredShotBuff = BuffType<Buffs.EmpoweredShotBuff>();
            if (heldItem != item && player.HasBuff(empoweredShotBuff)) {
                player.ClearBuff(empoweredShotBuff);
            }
        }

        public override void HoldItem(Player player) {
            float cursorDist = Vector2.Distance(Main.MouseWorld, player.position); // calculates cursor distance from the player every frame
            int empoweredShotBuff = BuffType<Buffs.EmpoweredShotBuff>();
            // Sets chakram mode and manage empowered shot buff while it's beeing held
            if (cursorDist < MeleeDist) {
                Mode = "Melee";
                timer = 0;
                if (player.HasBuff(empoweredShotBuff)) {
                    player.ClearBuff(empoweredShotBuff);
                }
            } else {
                Mode = "Ranged";
                if (timer < EmpoweredTreshold) {
                    timer++;
                } else if (!player.HasBuff(empoweredShotBuff)) {
                    player.AddBuff(empoweredShotBuff, 10000);
                }
            }

            DerivedHoldItem(player);
        }

        // Hold item for derived classes
        public virtual void DerivedHoldItem(Player player) {}

        public override bool Shoot(Player player, ref Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack) {
            timer = 0; // reset empowered timer when used
            int empoweredBuff = BuffType<Buffs.EmpoweredShotBuff>();
            bool empowered = player.HasBuff(empoweredBuff);
            bool ret = false;
            
            if (ChakramPlayer.ModPlayer(player).SpawnAtPlayer[type]) {
                if (empowered) {
                    Projectile.NewProjectile(player.Center.X, player.Center.Y, 0, 0, type, (int)(EmpoweredShotMult * damage), knockBack, player.whoAmI);
                    player.ClearBuff(empoweredBuff);
                } else {
                    Projectile.NewProjectile(player.Center.X, player.Center.Y, 0, 0, type, damage, knockBack, player.whoAmI);
                }
            } else {
                if (empowered) { // to do default
                    Projectile.NewProjectile(position.X, position.Y, speedX, speedY, type, (int)(EmpoweredShotMult * damage), knockBack, player.whoAmI);
                    player.ClearBuff(empoweredBuff);
                } else {
                    ret = true;
                }
            }

            return ret;
        }
    }
}