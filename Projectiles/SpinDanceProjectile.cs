﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using static Terraria.ModLoader.ModContent;
using Microsoft.Xna.Framework;

namespace CrossCode.Projectiles
{
    class SpinDanceProjectile : ModProjectile
    {
        public static bool SpawnAtPlayer { get; private set; } = true;
        private int orbitDist;
        private int orbitSpeed;
        private int orbitStart;
        private int fullRotationsCount;
        private int maxDegreesPositive;
        private int maxDegreesNegative;
        private int FullRotationsCount {
            get => fullRotationsCount;
            set {
                fullRotationsCount = value;
                maxDegreesPositive = orbitStart + (value * 360);
                maxDegreesNegative = orbitStart - (value * 360);
            }
        }
        bool facingRight;

        public override void SetDefaults() {
            projectile.height = 60;
            projectile.width = 70;
            projectile.aiStyle = -1;
            projectile.friendly = true;
            projectile.melee = true;
            projectile.timeLeft = 360;
            projectile.light = 0.5f;
            projectile.ignoreWater = true;
            projectile.tileCollide = false;
            projectile.penetrate = -1;

            orbitStart = -90;
            orbitDist = 50; // distance of the proj from the player
            orbitSpeed = 25; // speed of the rotation in degrees per ?frame?
            FullRotationsCount = 3;
        }

        public override void AI() {
            /* 
             * Rotates the projectile around the player by calculating cosinus and sinus from an angle going from 0 to 720 degreees (two rounds around player)
            */

            /*projectile.soundDelay--;
            if (projectile.soundDelay <= 0)//this is the proper sound delay for this type of weapon
            {
                Main.PlaySound(2, (int)projectile.Center.X, (int)projectile.Center.Y, 15);    //this is the sound when the weapon is used
                projectile.soundDelay = 45;    //this is the proper sound delay for this type of weapon
            }*/

            Player player = Main.player[projectile.owner];
            
            if (projectile.ai[0] == 0) {
                projectile.ai[0]++;
                projectile.ai[1] = orbitStart;
                facingRight = player.direction == 1;
            } else {
                Projectile dust = Projectile.NewProjectileDirect(projectile.position, Vector2.Zero, ProjectileType<Dusts.SpinDanceDust>(), 0, 0, player.whoAmI);
                dust.position = projectile.position;
                dust.rotation = projectile.rotation;
            }

            double angleRad = MathHelper.ToRadians(projectile.ai[1]);

            if (facingRight) {
                projectile.position.X = player.Center.X + ((float)Math.Cos(angleRad) * orbitDist) - (projectile.width / 2);
                projectile.position.Y = player.Center.Y + ((float)Math.Sin(angleRad) * orbitDist) - (projectile.height / 2);
                projectile.rotation = (float)angleRad + MathHelper.ToRadians(90f);
                projectile.ai[1] += orbitSpeed;

                if (projectile.ai[1] > maxDegreesPositive) {
                    projectile.Kill();
                }
            } else {
                projectile.position.X = player.Center.X + ((float)Math.Cos(angleRad) * orbitDist) - (projectile.width / 2);
                projectile.position.Y = player.Center.Y + ((float)Math.Sin(angleRad) * orbitDist) - (projectile.height / 2);
                projectile.rotation = (float)angleRad - MathHelper.ToRadians(90f);
                projectile.ai[1] -= orbitSpeed;

                if (projectile.ai[1] < maxDegreesNegative) {
                    projectile.Kill();
                }
            }
        }
    }
}
