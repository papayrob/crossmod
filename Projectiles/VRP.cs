﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria.ID;
using Terraria.ModLoader;

namespace CrossCode.Projectiles
{
    class VRP : ModProjectile
    {
        public static bool SpawnAtPlayer { get; private set; } = false;

        public override void SetStaticDefaults() {
            DisplayName.SetDefault("Virtual Ricochet Projectile");
        }

        public override void SetDefaults() {
            projectile.width = 8;
            projectile.height = 8;
            projectile.aiStyle = 1;
            projectile.friendly = true;
            projectile.ranged = true;
            projectile.timeLeft = 600;
            projectile.light = 0.5f;
            projectile.ignoreWater = true;
            projectile.tileCollide = true;
            aiType = ProjectileID.Bullet;
        }
    }
}
