using Terraria.ModLoader;
using CrossCode.Items.ChakramClass;

namespace CrossCode
{
	public class CrossCode : Mod
	{
		public static ModHotKey SpecialActionHotKey;

		public CrossCode() {
		}

		public override void Load() {
			Logger.InfoFormat("{0} example logging", Name);

			// Add damage modification for chakrams
			ChakramItem.AddHacks();

			SpecialActionHotKey = RegisterHotKey("Special action", "LeftShift");
		}
		
	}
}