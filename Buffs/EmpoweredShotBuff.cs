﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria.ModLoader;
using Terraria;

namespace CrossCode.Buffs
{
    class EmpoweredShotBuff : ModBuff
    {
        public override void SetDefaults() {
            DisplayName.SetDefault("Empowered shot");
            Description.SetDefault("Your next shot is empowered");
            Main.debuff[Type] = false;
        }
    }
}
