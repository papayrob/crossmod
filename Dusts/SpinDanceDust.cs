﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;

namespace CrossCode.Dusts
{
    class SpinDanceDust : ModProjectile
    {
        public override void SetStaticDefaults() {
            Main.projFrames[projectile.type] = 4;
        }

        public override void SetDefaults() {
            projectile.height = 60;
            projectile.width = 70;
            projectile.aiStyle = -1;
            projectile.friendly = false;
            projectile.hostile = false;
            projectile.timeLeft = 12;
            projectile.ignoreWater = true;
            projectile.tileCollide = false;
            projectile.penetrate = -1;
            projectile.frameCounter = 0;
        }

        public override bool CanDamage() {
            return false;
        }

        public override void AI() {
            projectile.frameCounter++;

            if (projectile.frameCounter >= 3) {
                projectile.frameCounter = 0;
                projectile.frame++;
                projectile.frame %= 4;
            }
        }
    }
}
